import pytest
import requests
import json
import random

URL_CREATE_CLIENT = 'http://localhost:8080/service/v1/client/create'
URL_CREATE_ORDER = 'http://localhost:8080/service/v1/order/create',
URL_CHECK_PURCHASES = 'http://localhost:8080/service/v1/item/purchase/by-client'

def make_test_client():
    """
    Make new test client
    """
    data = {
        "name": "test_client",
        "surname": "test_client",
        "phone": "".join([random.choice(list('1234567890')) for x in range(11)])
    }
    body = json.dumps(data)

    headers = {
        "Content-Type": "application/json"
    }
    resp = requests.request("POST", 
                        URL_CREATE_CLIENT,
                        headers=headers,
                        json=body,
                        timeout=120)
    return resp
    

def make_test_order(client_id, item_ids):
    """
    Make new test order
    param client_id: id of client
    param item_ids: ids of ordered items
    """
    items = [{
        "item_id": item_id,
        "price": 0.05,
        "quantity": 10
    } for item_id in item_ids]
    
    data = {
        "client_id": str(client_id),
        "address": "test_address",
        "phone": "".join([random.choice(list('1234567890')) for x in range(11)]),
        "items": items}
    body = json.dumps(data)

    headers = {
        "Content-Type": "application/json"
    }
    resp = requests.request("POST", 
                        URL_CREATE_ORDER,
                        headers=headers,
                        json=body,
                        timeout=120)
    return resp

def make_new_client_and_new_order(item_ids):
    new_client = make_test_client().json()
    id_new_client = new_client["client_id"]
    new_order = make_test_order(id_new_client, item_ids).json()
    id_new_order = new_order["order_id"]
    number_of_new_order = new_order["order_number"]
    return (id_new_client, id_new_order, number_of_new_order)

def check_purchase(client_id, item_ids):
    """
    Check orders for client and items
    param client_id: id of client
    param item_ids: ids of ordered items
    """
    # в request нужны строковые item_ids
    item_str_ids = [str(x) for x in item_ids]
    data = {
        "client_id": str(client_id),
        "items": item_str_ids}
    body = json.dumps(data)

    headers = {
        "Content-Type": "application/json"
    }
    resp = requests.request("GET", 
                        URL_CHECK_PURCHASES,
                        headers=headers,
                        json=body,
                        timeout=120)
    return resp