import pytest
import tests.conftest as conftest

@pytest.fixture(scope="module")
def make_new_client_and_order():
    item_ids = [1, 2]
    data_of_first_order = conftest.make_new_client_and_new_order(item_ids)
    return data_of_first_order, item_ids


def get_item_by_id(items, item_id):
    for item in items:
        if item["item_id"] == item_id:
            return item
    return None


@pytest.mark.parametrize("case", ["simple_positive", "unexistable_item", "unexistable_client", "unordered item", "one_more_order"])
def test_get_purchased_items_by_client(make_new_client_and_order, case):
    data_of_first_order, item_ids = make_new_client_and_order
    if case == "simple_positive":
        resp = conftest.check_purchase(data_of_first_order[0], item_ids)
        assert resp.status_code == 200
        first_item = get_item_by_id(resp.json()["items"], 1)
        assert first_item["item_id"] == "1"
        assert first_item["purchased"] == True
        assert first_item["last_order_number"] == data_of_first_order[2]
        assert first_item["purchase_count"] == 1
    elif case == "unexistable_item":
        resp = conftest.check_purchase(data_of_first_order[0], [-1])
        assert resp.status_code == 404
    elif case == "unexistable_client":
        resp = conftest.check_purchase("-1", [1, 2])
        assert resp.status_code == 404
    elif case == "unordered_item":
        resp = conftest.check_purchase(data_of_new_order[0], [3])
        assert resp.status_code == 200
        item_3 = get_item_by_id(resp.json()["items"], 3)
        assert item_3["purchased"] == False
        assert item_3["purchase_count"] == 0
    elif case == "one_more_order":
        data_of_new_order = conftest.make_test_order(data_of_first_order[0], [1, 3])
        resp_new_order = conftest.check_purchase(data_of_first_order[0], [1, 2, 3])
        assert resp_new_order.status_code == 200
        item_1 = get_item_by_id(resp_new_order.json()["items"], 1)
        item_2 = get_item_by_id(resp_new_order.json()["items"], 2)
        item_3 = get_item_by_id(resp_new_order.json()["items"], 3)
        assert item_1["purchased"] == True
        assert item_2["purchased"] == True
        assert item_3["purchased"] == True
        assert item_1["purchase_count"] == 2
        assert item_2["purchase_count"] == 1
        assert item_3["purchase_count"] == 1
        assert item_1["last_order_number"] == data_of_new_order["order_number"]
        assert item_2["last_order_number"] == data_of_first_order[2]
        assert item_1["last_order_number"] == data_of_new_order["order_number"]

# TODO: тесты, где одно из полей не указано/указано невалидно
# TODO: проверить даты
# TODO: тесты, где в списке item'ов смешаны существующие и несуществующие id




